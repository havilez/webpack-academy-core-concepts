const path = require("path"); // provided by node.js
const webpack = require("webpack");

const ExamplePlugin  = require('./ExamplePlugin');

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "bundle.js",
        path: path.join(__dirname,"dist"),
    },
    // add loaders
    module: {
        rules: [
            {
                test: /|.js$/,
                use: "babel-loader"
            },
            {
                test: /\.jpg$/,
                use: [ "file-loader"]
            }
            // {
            //     test: /\.css$/,
            //     use: [
            //         "style-loader",
            //         "css-loader",
            //         ]
            // },
            // {
            //     test: /\.jpeg$/,
            //     use: [
            //         {
            //             loader: {
            //                 "url-loader",
            //                 options: {
            //                     limit: 10000
            //                 } }
            //         }
            //     ]
            // }

        ]
    },
    plugins: [
        new ExamplePlugin(),
        // new webpack.optimize.UglifyjsPlugin()
    ]
}